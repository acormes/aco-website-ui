:toc-title:
:sectids:
:sectanchors:

= Avenir Cyclotourisme Ormes

Bienvenue sur le site internet de l'Avenir Cyclotourisme Ormes, club de :

- xref:route.adoc[Cyclo ROUTE]
- xref:vtt.adoc[Cyclo VTT]
- xref:gravel.adoc[Cyclo GRAVEL]

Retrouver tous les détails du club dans la section xref:club.adoc[Le club]


== Le *26 et 27 Avril 2025* - Rand'Ormoise - Challenge du Centre -

L'A.C Ormes cyclotourisme organise une nouvelle édition de la Rand'Ormoise 2025, 1er Challenge du Centre de l'année 2025

Venez découvrir les petites routes de l'Est Orléanais, les chemins VTT de la Canaudière et les grands chemins de la forêt d'Orléans

*https://www.billetweb.fr/randormoise-2025&language=fr[>>> Détails de l'évènement et inscription en ligne ici <<<]*

=== Route
- 55 kms
- 70 kms
- 98 kms
- Cyclo découverte le Samedi 26 Avril (~35 kms avec visite d'un moulin et d'un musée)

=== VTT
- 25 kms
- 45 kms
- 63 kms

=== Gravel
- 80 kms
- 110 kms

Trace GPX fournie quelques jours avant l'épreuve. +
GPS OBLIGATOIRE parcours non fléchés

=== Tarifs
- Licenciés FFCT : 5€
- Non licenciés : 8€
- -18 ans : Gratuit

=== Vidéo

Vous ne connaissez pas la Rand'Ormoise ? +
Découvrez ce qu'en pense les participants des précédentes versions :

> https://youtu.be/kbbhOE_jaaQ?feature=shared[Vidéo Mr Phal édition 2024^] +
> https://youtu.be/-6_NN00aWBU?feature=shared[Vidéo Plaisir Gravel Edition 2024^] +
> https://youtu.be/jamWm_f-lXg?feature=shared[Vidéo Mr Phal Edition 2023^] +

=== Informations pratiques

-  Ravitaillement sur tous les parcours
-  Repas le dimanche midi (Réservation obligatoire)
-  Parking pour les campings car (Réservation recommandée)

- Départ de 7h30 à 10h de la Salle des fêtes d'Ormes
** 149 Rue Nationale 45140 Ormes
** Inscriptions en ligne recommandée

- Accès :
** 4 kms de l'A10 sortie 14 Orléans Nord
** 10 kms de la gare SNCF des Aubrais
** Accéder au https://openstreetmap.org/directions?from=&to=47.942057%2C1.817347#map=13/47.93124/1.82236}[calculateur d'itinéraire^]

- Détails sur le site internet : https://acormes-cyclo.fr/
- Contacts et questions : acormes.cyclo@gmail.com


*https://www.billetweb.fr/randormoise-2025&language=fr[>>> Détails de l'évènement et inscription en ligne ici <<<]*

image::../images/randormoise_2025.png[randormoise, 300]


image:../images/randormoise_1_2025.png[randormoise, 245, 200]
image:../images/randormoise_2_2025.png[randormoise, 192, 200]
image:../images/randormoise_3_2025.png[randormoise, 160, 200]
image:../images/randormoise_4_2025.png[randormoise, 180, 200]



== Inscription - licence 2025

Voici toutes les informations pour la création / le renouvellement de la licence 2025 :

- xref:club.adoc#_inscription_au_club_pour_lannée_2025[Incription au club - Licence]


////

= Hardware and Software Requirements
Author Name
:idprefix:
:idseparator: -
:!example-caption:
:!table-caption:
:page-pagination:

[.float-group]
--
image:multirepo-ssg.svg[Multirepo SSG,180,135,float=right,role=float-gap]
Platonem complectitur mediocritatem ea eos.
Ei nonumy deseruisse ius.
Mel id omnes verear.
Vis no velit audiam, sonet <<dependencies,praesent>> eum ne.
*Prompta eripuit* nec ad.
Integer diam enim, dignissim eget eros et, ultricies mattis odio.
--

Vestibulum consectetur nec urna a luctus.
Quisque pharetra tristique arcu fringilla dapibus.
https://example.org[Curabitur,role=unresolved] ut massa aliquam, cursus enim et, accumsan lectus.
Mauris eget leo nunc, nec tempus mi? Curabitur id nisl mi, ut vulputate urna.

== Cu solet

Nominavi luptatum eos, an vim hinc philosophia intellegebat.
Lorem pertinacia `expetenda` et nec, [.underline]#wisi# illud [.line-through]#sonet# qui ea.
H~2~0.
E = mc^2^.
Eum an doctus <<liber-recusabo,maiestatis efficiantur>>.
Eu mea inani iriure.footnote:[Quisque porta facilisis tortor, vitae bibendum velit fringilla vitae! Lorem ipsum dolor sit amet, consectetur adipiscing elit.]

[,json]
----
{
  "name": "module-name",
  "version": "10.0.1",
  "description": "An example module to illustrate the usage of package.json",
  "author": "Author Name <author@example.com>",
  "scripts": {
    "test": "mocha",
    "lint": "eslint"
  }
}
----

.Example paragraph syntax
[,asciidoc]
----
.Optional title
[example]
This is an example paragraph.
----

.Optional title
[example]
This is an example paragraph.

.Summary *Spoiler Alert!*
[%collapsible]
====
Details.

Loads of details.
====

[,asciidoc]
----
Voila!
----

.Result
[%collapsible.result]
====
Voila!
====

=== Some Code

How about some code?

[,js]
----
vfs
  .src('js/vendor/*.js', { cwd: 'src', cwdbase: true, read: false })
  .pipe(tap((file) => { // <.>
    file.contents = browserify(file.relative, { basedir: 'src', detectGlobals: false }).bundle()
  }))
  .pipe(buffer()) // <.>
  .pipe(uglify())
  .pipe(gulp.dest('build'))
----
<.> The `tap` function is used to wiretap the data in the pipe.
<.> Wrap each streaming file in a buffer so the files can be processed by uglify.
Uglify can only work with buffers, not streams.

Execute these commands to validate and build your site:

 $ podman run -v $PWD:/antora:Z --rm -t antora/antora \
   version
 3.0.0
 $ podman run -v $PWD:/antora:Z --rm -it antora/antora \
   --clean \
   antora-playbook.yml

Cum dicat #putant# ne.
Est in <<inline,reque>> homero principes, meis deleniti mediocrem ad has.
Altera atomorum his ex, has cu elitr melius propriae.
Eos suscipit scaevola at.

....
pom.xml
src/
  main/
    java/
      HelloWorld.java
  test/
    java/
      HelloWorldTest.java
....

Eu mea munere vituperata constituam.

[%autowidth]
|===
|Input | Output | Example

m|"foo\nbar"
l|foo
bar
a|
[,ruby]
----
puts "foo\nbar"
----
|===

Here we just have some plain text.

[source]
----
plain text
----

[.rolename]
=== Liber recusabo

Select menu:File[Open Project] to open the project in your IDE.
Per ea btn:[Cancel] inimicus.
Ferri kbd:[F11] tacimates constituam sed ex, eu mea munere vituperata kbd:[Ctrl,T] constituam.

.Sidebar Title
****
Platonem complectitur mediocritatem ea eos.
Ei nonumy deseruisse ius.
Mel id omnes verear.

Altera atomorum his ex, has cu elitr melius propriae.
Eos suscipit scaevola at.
****

No sea, at invenire voluptaria mnesarchum has.
Ex nam suas nemore dignissim, vel apeirian democritum et.
At ornatus splendide sed, phaedrum omittantur usu an, vix an noster voluptatibus.

---

.Ordered list with customized numeration
[upperalpha]
. potenti donec cubilia tincidunt
. etiam pulvinar inceptos velit quisque aptent himenaeos
. lacus volutpat semper porttitor aliquet ornare primis nulla enim

Natum facilisis theophrastus an duo.
No sea, at invenire voluptaria mnesarchum has.

.Unordered list with customized marker
[square]
* ultricies sociosqu tristique integer
* lacus volutpat semper porttitor aliquet ornare primis nulla enim
* etiam pulvinar inceptos velit quisque aptent himenaeos

Eu sed antiopam gloriatur.
Ea mea agam graeci philosophia.

[circle]
* circles
** circles
*** and more circles!

At ornatus splendide sed, phaedrum omittantur usu an, vix an noster voluptatibus.

* [ ] todo
* [x] done!

Vis veri graeci legimus ad.

sed::
splendide sed

mea::
agam graeci

Let's look at that another way.

[horizontal]
sed::
splendide sed

mea::
agam graeci

At ornatus splendide sed.

.Library dependencies
[#dependencies%autowidth%footer,stripes=hover]
|===
|Library |Version

|eslint
|^1.7.3

|eslint-config-gulp
|^2.0.0

|expect
|^1.20.2

|istanbul
|^0.4.3

|istanbul-coveralls
|^1.0.3

|jscs
|^2.3.5

h|Total
|6
|===

Cum dicat putant ne.
Est in reque homero principes, meis deleniti mediocrem ad has.
Altera atomorum his ex, has cu elitr melius propriae.
Eos suscipit scaevola at.

[TIP]
This oughta do it!

Cum dicat putant ne.
Est in reque homero principes, meis deleniti mediocrem ad has.
Altera atomorum his ex, has cu elitr melius propriae.
Eos suscipit scaevola at.

[NOTE]
====
You've been down _this_ road before.
====

Cum dicat putant ne.
Est in reque homero principes, meis deleniti mediocrem ad has.
Altera atomorum his ex, has cu elitr melius propriae.
Eos suscipit scaevola at.

[WARNING]
====
Watch out!
====

[CAUTION]
====
[#inline]#I wouldn't try that if I were you.#
====

[IMPORTANT]
====
Don't forget this step!
====

.Key Points to Remember
[TIP]
====
If you installed the CLI and the default site generator globally, you can upgrade both of them with the same command.

 $ npm i -g @antora/cli @antora/site-generator

Or you can install the metapackage to upgrade both packages at once.

 $ npm i -g antora
====

Nominavi luptatum eos, an vim hinc philosophia intellegebat.
Eu mea inani iriure.

[discrete]
== Voluptua singulis

Cum dicat putant ne.
Est in reque homero principes, meis deleniti mediocrem ad has.
Ex nam suas nemore dignissim, vel apeirian democritum et.

.Antora is a multi-repo documentation site generator
image::multirepo-ssg.svg[Multirepo SSG,3000,opts=interactive]

Make the switch today!

.Full Circle with Jake Blauvelt
video::300817511[vimeo,640,360,align=left]

[#english+中文]
== English + 中文

Altera atomorum his ex, has cu elitr melius propriae.
Eos suscipit scaevola at.

[,'Famous Person. Cum dicat putant ne.','Cum dicat putant ne. https://example.com[Famous Person Website]']
____
Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Mauris eget leo nunc, nec tempus mi? Curabitur id nisl mi, ut vulputate urna.
Quisque porta facilisis tortor, vitae bibendum velit fringilla vitae!
Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Mauris eget leo nunc, nec tempus mi? Curabitur id nisl mi, ut vulputate urna.
Quisque porta facilisis tortor, vitae bibendum velit fringilla vitae!
____

Lorem ipsum dolor sit amet, consectetur adipiscing elit.

[verse]
____
The fog comes
on little cat feet.
____

== Fin

That's all, folks!
////

