:toc-title:
:sectids:
:sectanchors:

= Les vêtements du club

image::images/vetements1.png[Vêtements 1]

image::images/vetements2.png[Vêtements 2]

|===
| Vêtement                        | Prix

| Maillot manches courtes cyclo   | 36 €
| Maillot manches courtes pro     | 40 €
| Maillot manches longues hiver   | 60 €
| Veste thermique                 | 78 €
| Coupe-vent                      | 55 €
| Gilet sans manche               | 48 €
| Cuissard court                  | 48 €
| Cuissard court                  | 48 €
| Corsaire                        | 60 €
| Collant long                    | 69 €
|===
